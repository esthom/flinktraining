import sbt._
import Keys._
import Avro4sKeys._


name := "FlinkTraining"

version := "1.0"

scalaVersion := "2.11.8"

lazy val root = (project in file("."))
    .settings(libraryDependencies ++= Dependencies.flinkDependencies ++ Dependencies.elasticDependencies)
  .aggregate(flinkbasic, flinkwindows, flinkes, kafkaproducer, common)

//flink modules

lazy val flinkbasic = (project in file("modules/flinkbasic"))
  .settings(Common.settings: _*)
  .settings(libraryDependencies ++= Dependencies.flinkDependencies)
  .settings(
    mainClass in assembly := Some("io.esthom.flinkbasic.task04.Main")
  )
  .dependsOn(common)

lazy val flinkwindows = (project in file("modules/flinkwindows"))
  .settings(Common.settings: _*)
  .disablePlugins(Avro4sSbtPlugin)
  .settings(libraryDependencies ++= Dependencies.flinkDependencies)
  .dependsOn(common, flinkbasic)

//kafka modules

lazy val kafkaproducer = (project in file("modules/kafkaproducer"))
  .settings(Common.settings: _*)
  .disablePlugins(Avro4sSbtPlugin)
  .settings(libraryDependencies ++= Dependencies.kafkaDependencies)
  .dependsOn(common)

lazy val flinkes = (project in file("modules/flinkes"))
  .settings(Common.settings: _*)
  .disablePlugins(Avro4sSbtPlugin)
  .settings(libraryDependencies ++= Dependencies.flinkDependencies ++ Dependencies.elasticDependencies)
  .dependsOn(common, flinkbasic, flinkwindows)

// common

lazy val common = (project in file("modules/common"))
  .settings(Common.settings: _*)
  .settings(libraryDependencies ++= Dependencies.commonDependencies)
