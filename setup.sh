#!/usr/bin/env bash

set +x

git checkout wsh

INDEX_NAME="cityrating_$(whoami)"

echo "Creating new ElasticSearch index: $INDEX_NAME"

http PUT "$ES/$INDEX_NAME"
curl -XPUT "$ES/$INDEX_NAME/_mapping/city?pretty" -d'
{
            "properties" : {
                "city_name": { "type" : "string" },
                "rating" : { "type" : "double" }
            }
        }
'

echo "Index: $INDEX_NAME should be available."

set -x