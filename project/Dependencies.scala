import sbt._
import Keys._

object Dependencies {

  val flinkVersion = "1.1.3"
  val kafkaVersion = "0.9.0.0"
  val avro4sVersion = "1.6.3"
  val configVersion = "1.2.1"

  val commonDependencies = Seq(
    "com.sksamuel.avro4s" %% "avro4s-core" % avro4sVersion,
    "com.typesafe" % "config" % configVersion,
    "org.json4s" %% "json4s-jackson" % "3.5.0",
    "joda-time" % "joda-time" % "2.9.7"
  )

  val flinkDependencies = Seq(
    "org.apache.flink" %% "flink-scala",
    "org.apache.flink" %% "flink-streaming-scala",
    "org.apache.flink" % "flink-connector-kafka-0.9_2.10",
    "org.apache.flink" % "flink-connector-elasticsearch_2.10",
    "org.apache.flink" % "flink-connector-filesystem_2.10"
  ).map(_ % flinkVersion)

  val kafkaDependencies = Seq(
    "org.apache.kafka" %% "kafka"
  ).map(_ % kafkaVersion)

  val elasticDependencies = Seq(
    "com.sksamuel.elastic4s" %% "elastic4s-core" % "5.1.5",
    "joda-time" % "joda-time" % "2.9.7"
  )

}