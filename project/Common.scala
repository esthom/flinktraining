import sbt._
import Keys._

object Common {
  val settings =
    Seq(
      organization := "io.esthom",
      version := "1.0",
      scalaVersion := "2.11.8"
    )
}