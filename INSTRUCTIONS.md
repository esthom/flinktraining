# Create a kafka topic
./bin/kafka-topics --create --zookeeper cdh-edge-1.gid:2181/kafka --partitions 1 --replication-factor 1 --topic inittest

# Verify topic is registered and available
./bin/kafka-topics --zookeeper cdh-edge-1.gid:2181/kafka --list

# Run a consumer
./bin/kafka-console-consumer --bootstrap-server localhost:9092 --topic inittest --from-beginning --zookeeper cdh-edge-1.gid:2181/kafka

# Run a producer
bin/kafka-console-producer --broker-list cdh-edge-1.gid:9092 --topic inittest

# edge.c.getindata.internal=10.142.0.3

export KAFKA=/opt/cloudera/parcels/KAFKA/

$KAFKA/bin/kafka-topics --create --zookeeper $ZOOKEEPER --partitions 1 --replication-factor 1 --topic $KAFKA_TOPIC
$KAFKA/bin/kafka-topics --zookeeper $ZOOKEEPER --list

$KAFKA/bin/kafka-console-producer --broker-list 10.142.0.3:9092 --topic inittest
$KAFKA/bin/kafka-console-consumer --bootstrap-server 10.142.0.3:9092 --topic inittest --from-beginning --zookeeper 10.142.0.3:2181


Kibana
http://35.185.10.173:5601/app/kibana