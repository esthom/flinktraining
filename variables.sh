#!/usr/bin/env bash

echo "Setting all required environmental variables..."

export KAFKA=/opt/cloudera/parcels/KAFKA/

echo ""
echo "\$KAFKA variable: $KAFKA"

export ZOOKEEPER="$(hostname).$(dnsdomainname):2181"
export BOOTSTRAP_SERVERS="$(hostname).$(dnsdomainname):9092"
export KAFKA_TOPIC="cityrating"
export KAFKA_TOPIC_STRING="cityrating_string"
export KAFKA_PRIVATE="private_$(whoami)"
export GROUP_ID="group_$(whoami)"

echo ""
echo "\$ZOOKEEPER variable: $ZOOKEEPER"
echo "\$BOOTSTRAP_SERVERS variable: $BOOTSTRAP_SERVERS"
echo "\$KAFKA_TOPIC variable: $KAFKA_TOPIC"

export ITEM_REPOSITORY="$(pwd)/data/items.json"
export USER_REPOSITORY="$(pwd)/data/users.json"

echo ""
echo "\$ITEM_REPOSITORY variable: $ITEM_REPOSITORY"
echo "\$ITEM_REPOSITORY variable: $ITEM_REPOSITORY"

export INDEX_NAME="cityrating_$(whoami)"
export ES="104.196.69.91:9200"
export ES_CONNECTION="elasticsearch://104.196.69.91:9300?cluster.name=training-cluster"

echo ""
echo "\$INDEX_NAME variable: $INDEX_NAME"
echo "\$ES variable: $ES"
echo "\$ES_CONNECTION variable: $ES_CONNECTION"