package io.esthom.kafkaproducer.task04

import java.util.Properties

import io.esthom.common.avro.AvroFactory
import io.esthom.common.configuration.Config
import io.esthom.common.random.RatingGenerator
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

object Main {

  def main(args: Array[String]): Unit = {

    val props = new Properties()
    props.put("bootstrap.servers", Config.bootstrapServers)
    props.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer")

    val producer = new KafkaProducer[Int, Array[Byte]](props)

    sys.addShutdownHook{
      println()
      println("Closing the application! Producer will no longer send messages to kafka brokers!")
      producer.close()
    }

    println("\n\nIn order to close the producer hit CTRL+C.\n\n")

    while(true) {
      producer.send(new ProducerRecord(Config.topic, 0, AvroFactory.avroToBytes(RatingGenerator.generateRating)))
      Thread.sleep(Config.messageDeliveryTime)
    }

  }

}
