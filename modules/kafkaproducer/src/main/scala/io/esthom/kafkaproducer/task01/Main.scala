package io.esthom.kafkaproducer.task01

import java.util.Properties

import io.esthom.common.configuration.Config
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

import scala.util.Random


object Main {

  def main(args: Array[String]): Unit = {

    val props = new Properties()
    props.put("bootstrap.servers", Config.bootstrapServers)
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)

    val rand = new Random()

    sys.addShutdownHook{
      println()
      println("Closing the application! Producer will no longer send messages to kafka brokers!")
      producer.close()
    }

    println("\n\nIn order to close the producer hit CTRL+C.\n\n")

    while(true) {
      producer.send(new ProducerRecord(Config.topicString, "a", rand.nextInt(100).toString))
      Thread.sleep(500)
    }

  }

}
