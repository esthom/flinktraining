package io.esthom.kafkaproducer.task02

import java.util.Properties

import io.esthom.common.avro.AvroFactory
import io.esthom.common.configuration.Config
import io.esthom.common.random.RatingGenerator
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

object Main {

  def main(args: Array[String]): Unit = {

    val props = new Properties()
    props.put("bootstrap.servers", Config.bootstrapServers)
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)

    for(s <- (1 to 100).toList.map(_.toString)) {
      producer.send(new ProducerRecord(Config.topic, s, AvroFactory.avroToJSON(RatingGenerator.generateRating)))
    }

    producer.close()

  }
}
