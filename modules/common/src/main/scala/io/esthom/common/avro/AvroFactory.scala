package io.esthom.common.avro

import java.io.PrintWriter

import io.esthom.common.data.model.Container


object AvroFactory {

  import AvroTransformers.AvroTransformer

  def createAvroFile(path: String, schema: String): Unit = {
    new PrintWriter(path) { write(schema); close() }
  }

  def avroToBytes[T <: Container](data: T)(implicit trans: AvroTransformer[T]): Array[Byte] = {
    trans.avroToByte(data)
  }

  def avroToJSON[T <: Container](data: T)(implicit trans: AvroTransformer[T]): String = {
    trans.avroToJSON(data)
  }

  def byteToAvro[T <: Container](data: Array[Byte])(implicit trans: AvroTransformer[T]): Option[T] = {
    trans.toAvro(data)
  }

  def jsonToAvro[T <: Container](data: String)(implicit trans: AvroTransformer[T]): Option[T] = {
    trans.toAvro(data)
  }

}
