package io.esthom.common.data.aggregates

import io.esthom.common.data.model.{Item, User}

/**
  * Created by tomek on 20.12.2016.
  */
sealed trait Aggregate

case class UserAgg(user: User, items: List[Item], rating: Double) extends Aggregate
