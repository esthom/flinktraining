package io.esthom.common.configuration

import com.typesafe.config.ConfigFactory

object Config {

  val conf = ConfigFactory.load()
  val bootstrapServers = conf.getString("kafka.bootstrap.servers")
  val groupId = conf.getString("kafka.group.id")
  val topic = conf.getString("kafka.topic")
  val topicString = conf.getString("kafka.topicString")
  val userRepo = conf.getString("repositories.user")
  val itemRepo = conf.getString("repositories.item")
  val messageDeliveryTime = conf.getInt("kafka.lag")
  val esConnection = conf.getString("es.connection")
  val indexName = conf.getString("es.index")

}
