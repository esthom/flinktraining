package io.esthom.common.repositories

import java.util.Random

import io.esthom.common.configuration.Config
import io.esthom.common.data.model.User
import org.json4s._
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.read

import scala.io.Source


object UserRepository {

  implicit val formats = Serialization.formats(NoTypeHints)

  private val random = new Random()


  def getUsers(path: String): Vector[User] = {

    val userJson = Source.fromFile(
      path)
      .getLines().toList.mkString("")

    read[List[User]](userJson).toVector

  }

  def getRandomUser: User = {
    val users = getUsers(Config.userRepo)
    users(random.nextInt(users.length))
  }

}
