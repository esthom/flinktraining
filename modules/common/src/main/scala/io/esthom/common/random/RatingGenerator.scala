package io.esthom.common.random

import java.util.Random

import io.esthom.common.data.model.Rating
import io.esthom.common.repositories.{ItemRepository, UserRepository}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

object RatingGenerator {

  val df = DateTimeFormat
    .forPattern("yyyy-MM-dd'T'HH:mm:ssZZ")

  def generateRating: Rating = {
    val rnd = new Random()
    Rating(df.print(new DateTime()),
      UserRepository.getRandomUser,
      ItemRepository.getRandomItem,
      rnd.nextInt(5) + 1)
  }

}
