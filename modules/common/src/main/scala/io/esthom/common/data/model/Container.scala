package io.esthom.common.data.model

sealed trait Container

case class User(id: Long, name: String) extends Container
case class Item(id: Long, city: String) extends Container
case class Rating(datetime: String, user: User, item: Item, rating: Int) extends Container
