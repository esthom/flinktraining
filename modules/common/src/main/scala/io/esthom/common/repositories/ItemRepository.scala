package io.esthom.common.repositories

import java.util.Random

import io.esthom.common.configuration.Config
import io.esthom.common.data.model.Item
import org.json4s.NoTypeHints
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.read

import scala.io.Source


object ItemRepository {

  implicit val formats = Serialization.formats(NoTypeHints)

  private val random = new Random()

  def getItems(path: String): Vector[Item] = {

    val itemJson = Source.fromFile(path)
      .getLines().toList.mkString("")

    read[List[Item]](itemJson).toVector

  }

  def getRandomItem: Item = {
    val items = getItems(Config.itemRepo)
    items(random.nextInt(items.length))
  }

}
