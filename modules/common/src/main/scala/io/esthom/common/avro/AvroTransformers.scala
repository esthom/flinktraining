package io.esthom.common.avro

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}

import com.sksamuel.avro4s.{AvroInputStream, AvroJsonInputStream, AvroOutputStream}
import io.esthom.common.data.model.{Container, Rating}


object AvroTransformers {

  trait AvroTransformer[A <: Container] extends Serializable {
    def avroToByte(data: A): Array[Byte]

    def avroToJSON(data: A): String

    def toAvro(data: Array[Byte]): Option[A]

    def toAvro(data: String): Option[A]
  }

  object AvroTransformer {

    private def fromAvro[A <: Container](data: A)
                                        (f: ByteArrayOutputStream => AvroOutputStream[A]): ByteArrayOutputStream = {
      val baos = new ByteArrayOutputStream()
      val output = f(baos)
      output.write(data)
      output.close()
      baos
    }

    private def dataToAvro[A <: Container](data: Array[Byte])
                                          (f: ByteArrayInputStream => AvroInputStream[A]): AvroInputStream[A] = {
      val in = new ByteArrayInputStream(data)
      f(in)
    }


    implicit object RatingAvroByteTransformer extends AvroTransformer[Rating] {

      def avroToByte(data: Rating): Array[Byte] = {
        fromAvro(data)(AvroOutputStream.binary[Rating]).toByteArray
      }

      def avroToJSON(data: Rating): String = {
        fromAvro(data)(AvroOutputStream.json[Rating]).toString("UTF-8")
      }

      def toAvro(data: Array[Byte]): Option[Rating] = {
        dataToAvro(data)(AvroInputStream.binary[Rating]).iterator.toSeq.headOption
      }

      def toAvro(data: String): Option[Rating] = {
        dataToAvro(data.getBytes("UTF-8"))(AvroInputStream.json[Rating])
          .asInstanceOf[AvroJsonInputStream[Rating]]
          .singleEntity.toOption
      }

    }

  }

}
