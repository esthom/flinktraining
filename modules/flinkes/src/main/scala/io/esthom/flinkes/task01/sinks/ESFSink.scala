package io.esthom.flinkes.task01.sinks

import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.{ElasticClient, ElasticsearchClientUri}
import io.esthom.flinkwindows.task03.aggregates.MeanRatingPerCity
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class ESFSink(connectionString: String, indexName: String) extends RichSinkFunction[MeanRatingPerCity] {

  private var elastic: Option[ElasticClient] = None

  override def open(parameters: Configuration): Unit = {
    elastic = Some(ElasticClient.transport(
      ElasticsearchClientUri(connectionString)
    ))
  }

  override def invoke(element: MeanRatingPerCity): Unit = {
    val df = DateTimeFormat
      .forPattern("yyyy-MM-dd'T'HH:mm:ssZZ")

    elastic match {
      case Some(connection) => connection.execute(index into indexName -> "city" fields(
        "city_name" -> element.name,
        "rating" -> element.mean.toString,
        "post_date" -> df.print(new DateTime())
      ))
      case None => throw new Exception("The client ES instance is not available.")
    }

  }

  override def close(): Unit = {
    elastic match {
      case Some(connection) => connection.close()
      case None => throw new Exception("Cannot close ES client which is not available.")
    }
  }

}
