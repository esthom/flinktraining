package io.esthom.flinkes.task01

import java.util.Properties

import io.esthom.common.configuration.Config
import io.esthom.common.data.model.{Item, Rating}
import io.esthom.flinkbasic.task02.serde.AvroDeserializationSchema
import io.esthom.flinkes.task01.sinks.ESFSink
import io.esthom.flinkwindows.task03.aggregates.{Intermediate, MeanRatingPerCity}
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09

object Main {

  def main(args: Array[String]): Unit = {

    import org.apache.flink.api.scala._


    // creating execution environment that registers all sources or sinks, etc. on stream
    val env = StreamExecutionEnvironment.createLocalEnvironment()
    env.setParallelism(2)
    // enabling checkpointing for recovery
    env.enableCheckpointing(5000)
    env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime)

    // building properties for a kafka consumer
    val props = new Properties()
    props.setProperty("bootstrap.servers", Config.bootstrapServers)
    props.setProperty("group.id", Config.groupId)


    //    val schema: DeserializationSchema
    // registering a kafka consumer (for kafka 0.9.x) and a sink (printing to console)
    val stream = env
      .addSource(new FlinkKafkaConsumer09(Config.topic, new AvroDeserializationSchema[Rating], props))

    val aggregate = stream
      .keyBy(_.item)
      .timeWindow(Time.seconds(30))
      .fold(Intermediate(0, 0, "")) {
        case (inter, Rating(_, _, Item(_, name), rating)) =>
          inter.copy(inter.sum + rating, inter.count + 1, name)
      }
      .map(inter =>
        MeanRatingPerCity(inter.name ,
          BigDecimal(inter.sum.toDouble / inter.count)
            .setScale(2, BigDecimal.RoundingMode.HALF_UP)
            .toDouble)
      )

    aggregate.addSink(new ESFSink(Config.esConnection, Config.indexName))

    aggregate.print()

    println(env.getExecutionPlan)

    env.execute

  }

}
