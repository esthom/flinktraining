package io.esthom.flinkbasic.task02

import java.util.Properties

import io.esthom.common.configuration.Config
import io.esthom.common.data.model.Rating
import io.esthom.flinkbasic.task02.serde.AvroDeserializationSchema
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09
import org.apache.flink.streaming.util.serialization.{DeserializationSchema, SimpleStringSchema}

object Main {

  def main(args: Array[String]): Unit = {

    import org.apache.flink.api.scala._


    // creating execution environment that registers all sources or sinks, etc. on stream
    val env = StreamExecutionEnvironment.createLocalEnvironment()
    env.setParallelism(2)
    // enabling checkpointing for recovery
    env.enableCheckpointing(5000)

    // building properties for a kafka consumer
    val props = new Properties()
    props.setProperty("bootstrap.servers", Config.bootstrapServers)
    props.setProperty("group.id", Config.groupId)


//    val schema: DeserializationSchema
    // registering a kafka consumer (for kafka 0.9.x) and a sink (printing to console)
    val stream = env
      .addSource(new FlinkKafkaConsumer09(Config.topic, new AvroDeserializationSchema[Rating], props))
      .print()

    // executing the flow
    env.execute

  }

}
