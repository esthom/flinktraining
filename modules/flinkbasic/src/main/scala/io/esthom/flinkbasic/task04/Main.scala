package io.esthom.flinkbasic.task04

import java.util.Properties

import io.esthom.common.configuration.Config
import io.esthom.common.data.model.{Rating, User}
import io.esthom.flinkbasic.task02.serde.AvroDeserializationSchema
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09


object Main {

  def main(args: Array[String]): Unit = {

    import org.apache.flink.api.scala._

    /** This line allows to change some popular transforming methods on DataStream so they accept
      * Scala partial functions syntax and allow to destructure case classes
      *
      * Examples:
      * .map() -> .mapWith()
      * .filter() -> mapFilter()
      *
      * This syntax facilitates writing clean, comprehensible code.
      */
    import org.apache.flink.streaming.api.scala.extensions._


    val env = StreamExecutionEnvironment.createLocalEnvironment()

    env.setParallelism(2)

    env.enableCheckpointing(5000)

    val props = new Properties()
    props.setProperty("bootstrap.servers", Config.bootstrapServers)
    props.setProperty("group.id", Config.groupId)

    val stream = env
      .addSource(new FlinkKafkaConsumer09(Config.topic, new AvroDeserializationSchema[Rating], props))

    /**
      * map and filter functions changed to mapWith and filterWith in order to utilize partial functions
      * and case class destructuring
      */
    val stream1 = stream
      .filterWith {
        _.user.id >= 1005
      }.name("This is a filter step >= 1005")
      .mapWith {
        case Rating(_, User(id, _), _, rating) => ("Stream 1", id, rating)
      }.name("This is a map step >= 1005")

    val stream2 = stream
      .filterWith{
        _.user.id < 1005
      }.name("This is a filter step < 1005")
      .mapWith{
        case Rating(_, User(id, _), _, rating) => ("Stream 2", id, rating)
      }.name("This is a map step  < 1005")

    stream1.union(stream2).print().name("Console sink")

    println(env.getExecutionPlan)

    env.execute

  }

}
