package io.esthom.flinkbasic.task03

import java.util.Properties

import io.esthom.common.configuration.Config
import io.esthom.common.data.model.{Rating, User}
import io.esthom.flinkbasic.task02.serde.AvroDeserializationSchema
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09


object Main {

  def main(args: Array[String]): Unit = {

    import org.apache.flink.api.scala._

    /** This exercise shows how to use some basic function on DataStream in order to manipulate the flow of data
      *
      */
    val env = StreamExecutionEnvironment.createLocalEnvironment()

    env.setParallelism(2)

    env.enableCheckpointing(5000)

    val props = new Properties()
    props.setProperty("bootstrap.servers", Config.bootstrapServers)
    props.setProperty("group.id", Config.groupId)

    val stream = env
      .addSource(new FlinkKafkaConsumer09(Config.topic, new AvroDeserializationSchema[Rating], props))

    val stream1 = stream
      .filter{ _.user.id > 1012 }.name("This is a filter step >= 1005")
      .map{rating => ("Stream 1", rating.user.id, rating.rating)}.name("This is a map step >= 1005")

    val stream2 = stream
      .filter{ _.user.id < 1012 }.name("This is a filter step < 1005")
      .map{ rating => ("Stream 2", rating.user.id, rating.rating)}.name("This is a map step  < 1005")

    stream1.union(stream2).print().name("Console sink")

    println(env.getExecutionPlan)

    env.execute

  }

}
