package io.esthom.flinkbasic.task02.serde

import io.esthom.common.avro.AvroTransformers
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.typeutils.TypeExtractor
import org.apache.flink.streaming.util.serialization.DeserializationSchema

import scala.reflect.ClassTag
import AvroTransformers.AvroTransformer
import io.esthom.common.data.model.Container

class AvroDeserializationSchema[T <: Container](implicit tag: ClassTag[T],
                                                trans: AvroTransformer[T]) extends DeserializationSchema[T] {

  def deserialize(message: Array[Byte]): T = {

    trans.toAvro(message).get

  }

  def isEndOfStream(element: T): Boolean = {
    false
  }

  def getProducedType: TypeInformation[T] = {
    TypeExtractor.getForClass(tag.runtimeClass.asInstanceOf[Class[T]])
  }

}
