package io.esthom.flinkwindows.task03.aggregates


sealed trait StreamAggregate

case class MeanRatingPerCity(name: String, mean: Double) extends StreamAggregate
case class Intermediate(sum: Int, count: Int, name: String) extends StreamAggregate


