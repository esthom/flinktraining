package io.esthom.flinkwindows.task01.aggregates

import io.esthom.common.data.aggregates.UserAgg
import io.esthom.common.data.model.{Item, Rating, User}
import org.apache.flink.streaming.api.scala.function.WindowFunction
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector


object AggregateFunctions {

  class UserAggregateFunction extends WindowFunction[Rating, UserAgg, User, TimeWindow] {

    def apply(key: User, window: TimeWindow, input: Iterable[Rating], out: Collector[UserAgg]): Unit = {
      val rating: Double = input.map(_.rating).sum
      val items: List[Item] = input.map(_.item).toList.distinct
      out.collect(UserAgg(key, items, rating/items.length))
    }

  }

}
